function(db, Dbui, $stateParams, $scope, $timeout, DbuiAction) {
    console.log('loading calendar customPage', $stateParams);
    
    const className = $stateParams.className;
    const perspective = $stateParams.perspective || 'default';
    var perspectiveObj;
    
    
    const eventSource = function(p, callback, failCallback) {
        console.log('GET EVENTS', p);
        var startDate = p.startStr;
        var endDate = p.endStr;
        
        const dateField = perspectiveObj.dateField;
        const titleField = perspectiveObj.titleField;
        
        var queryObj = { $and:[
            {[dateField]:{$gt:startDate}},
            {[dateField]:{$lt:endDate}}
        ]};
                
        var eventObjects = db[className].find(queryObj);
        eventObjects.$promise.then(function() {
            
            var eventArr = [];
            
            _.forEach(eventObjects, function(obj) {
                
                eventArr.push({
                    title:obj[titleField],
                    start:obj[dateField],
                    allDay:true,
                    // className:p.cssClass,
                    theObject:obj
                });
                
            });
            
            // console.log(eventArr);
            callback(eventArr);
        });
    }
    
    
    
    const onEventClick = function(evtClickInfo) {
        
        console.log('EVENT CLICK', evtClickInfo);
        let props = evtClickInfo.event.extendedProps;
        
        if(this === 'show_summary') {
            $scope.selectedObject = props.theObject;
            $scope.$digest();
        }
        else {
            console.log('invoking', this);
            DbuiAction.invokeAction(perspectiveObj, props.theObject, this);
        }
    };
    
    $scope.loading = true;
    Dbui.getPerspective(perspective, className, 'calendar').then(p=>{
        perspectiveObj = $scope.perspective = p;
        
        const calendarConfig = {
            initialView: 'dayGridMonth',
            events:eventSource,
            eventClick:onEventClick
        };
        
        if(p.calendarConfig) {
            _.assign(calendarConfig, p.calendarConfig);
        }
        
        if(p.onEventSelect) {
            calendarConfig.eventClick = onEventClick.bind(p.onEventSelect);
        }
        
        
        try {
            $timeout( function() {
                var calendarEl = document.getElementById('workshopCalendar');
                console.log('calendarEl', calendarEl);
                var calendar = new FullCalendar.Calendar(mainCalendar, calendarConfig);
                calendar.render();
                $scope.loading = false;
            }, 100);
        } catch(err) {
            console.error('FullCalendar init error:', err, calendarConfig);
            DbuiAlert.danger('FullCalendar init failed (see log)');
        }
    });
    
}